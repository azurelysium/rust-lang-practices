
use std::collections::HashMap;
use std::f32::NAN;

#[derive(Debug)]
enum Elem {
    Nil,
    Number(f32),
    Op(fn(f32, f32) -> f32),
}

impl PartialEq for Elem {
    fn eq(&self, other: &Elem) -> bool {
        match self {
            Elem::Number(n1) => {
                match other {
                    Elem::Number(n2) => {
                        return n1 == n2;
                    },
                    _ => { return false; },
                }
            },
            Elem::Op(f1) => {
                match other {
                    Elem::Op(f2) => {
                        return f1(1.0, 2.0) == f2(1.0, 2.0);
                    },
                    _ => { return false; },
                }
            },
            Elem::Nil => {
                match other {
                    Elem::Nil => { return true; },
                    _ => { return false; },
                }
            }
        }
    }
}

pub struct KorCalc<'a> {
    ops: Vec<(&'a str, fn(f32, f32) -> f32)>,
    digits: HashMap<&'a str , f32>,
    units: HashMap<&'a str, f32>,
}

impl<'a> KorCalc<'a> {
    pub fn new() -> KorCalc<'a> {
        KorCalc{
            ops: vec![
                ("더하기", |a, b| a + b),
                ("빼기", |a, b| a - b),
                ("곱하기", |a, b| a * b),
                ("나누기", |a, b| a / b),
            ],
            digits: [
                ("일", 1.0),
                ("이", 2.0),
                ("삼", 3.0),
                ("사", 4.0),
                ("오", 5.0),
                ("육", 6.0),
                ("칠", 7.0),
                ("팔", 8.0),
                ("구", 9.0),
            ].iter().cloned().collect(),
            units: [
                ("십", 10.0),
                ("백", 100.0),
                ("천", 1000.0),
                ("만", 10000.0),
            ].iter().cloned().collect(),
        }
    }

    fn tokenize<'x>(&self, statement: &'x str) -> Vec<&'x str> {
        let mut tokens = Vec::new();
        for (op, _) in &self.ops {
            match statement.find(op) {
                Some(idx) => {
                    let delimiter_len = op.len();
                    for tok in self.tokenize(&statement[0..idx]) {
                        tokens.push(tok);
                    }
                    tokens.push(&statement[idx..idx+delimiter_len]);
                    for tok in self.tokenize(&statement[idx+delimiter_len..]) {
                        tokens.push(tok);
                    }
                    break;
                },
                None => {}
            }
        }
        if tokens.is_empty() {
            tokens.push(statement);
        }
        tokens
    }

    fn translate(&self, token: &str) -> Elem {
        // for operators
        for (op, lambda) in &self.ops {
            if String::from(*op) == String::from(token) {
                return Elem::Op(*lambda);
            }
        }

        // for numbers
        let mut last_value: f32 = -1.0;
        let mut last_is_digit: bool = false;
        let mut number: f32 = 0.0;

        for c in token.chars() {
            let key: &str = &c.to_string();

            match self.units.get(key) {
                Some(unit) => {
                    if last_is_digit {
                        number += last_value * unit;
                    } else {
                        number += unit;
                    }
                    last_is_digit = false;
                    continue;
                },
                None => {},
            }
            match self.digits.get(key) {
                Some(digit) => {
                    if last_is_digit {
                        return Elem::Nil;
                    }
                    last_value = *digit;
                    last_is_digit = true;
                    continue;
                },
                None => {},
            }
            return Elem::Nil;
        }
        if last_is_digit {
            number += last_value;
        }

        if number > 0.0 {
            return Elem::Number(number);
        }
        Elem::Nil
    }

    fn parse(&self, statement: &str) -> Option<Vec<Elem>> {
        let statement_without_spaces = &statement.replace(" ", "");
        let tokens = self.tokenize(statement_without_spaces);
        let mut parsed = vec![];
        for token in tokens {
            let translated = self.translate(token);
            match translated {
                Elem::Nil => { return None; },
                _ => { parsed.push(translated); },
            }
        }
        return Some(parsed);
    }

    fn evaluate(&self, parsed: Vec<Elem>) -> Option<f32> {
        let mut stack: Vec<f32> = vec![];
        let mut op: Elem = Elem::Nil;

        for elem in parsed {
            match elem {
                Elem::Number(n) => {
                    stack.push(n);
                    if stack.len() == 2 {
                        if let Elem::Op(f) = op {
                            let result = f(stack[0], stack[1]);
                            stack.pop();
                            stack.pop();
                            stack.push(result);
                        } else {
                            return None;
                        }
                    }
                },
                Elem::Op(f) => {
                    op = Elem::Op(f);
                },
                _ => {},                    
            }
        }

        if stack.len() == 1 {
            return Some(stack[0]);
        }
        return None;
    }

    pub fn compute(&self, statement: &str) -> f32 {
        if let Some(parsed) = self.parse(statement) {
            match self.evaluate(parsed) {
                Some(result) => {
                    return result;
                },
                _ => { return NAN; }
            }
        } else {
            return NAN;
        }
    }
}

#[cfg(test)]
mod tests {
    use Elem;
    use KorCalc;

    #[test]
    fn instantiation_test() {
        KorCalc::new();
    }

    #[test]
    fn tokenize_test() {
        let calc = KorCalc::new();

        let tokens = calc.tokenize("백더하기일");
        assert_eq!(tokens, &["백", "더하기", "일"]);

        let tokens = calc.tokenize("십더하기오더하기일");
        assert_eq!(tokens, &["십", "더하기", "오", "더하기", "일"]);

        let tokens = calc.tokenize("십더하기오빼기일");
        assert_eq!(tokens, &["십", "더하기", "오", "빼기", "일"]);

        let tokens = calc.tokenize("십더하기오빼기일곱하기육십이");
        assert_eq!(tokens, &["십", "더하기", "오", "빼기", "일", "곱하기", "육십이"]);

        let tokens = calc.tokenize("십더하기오빼기일곱하기육십이나누기삼");
        assert_eq!(tokens, &["십", "더하기", "오", "빼기", "일", "곱하기", "육십이", "나누기", "삼"]);

        let tokens = calc.tokenize("사십빼기육십더하기오십");
        assert_eq!(tokens, &["사십", "빼기", "육십", "더하기", "오십"]);
    }

    #[test]
    fn translate_test() {
        let calc = KorCalc::new();

        assert_eq!(calc.translate("아무것"), Elem::Nil);

        assert_eq!(calc.translate("더하기"), Elem::Op(|a, b| a + b));
        assert_eq!(calc.translate("빼기"), Elem::Op(|a, b| a - b));
        assert_eq!(calc.translate("곱하기"), Elem::Op(|a, b| a * b));
        assert_eq!(calc.translate("나누기"), Elem::Op(|a, b| a / b));

        assert_eq!(calc.translate("일"), Elem::Number(1.0));
        assert_eq!(calc.translate("이십오"), Elem::Number(25.0));
        assert_eq!(calc.translate("십"), Elem::Number(10.0));
        assert_eq!(calc.translate("오십"), Elem::Number(50.0));
        assert_eq!(calc.translate("이만천구십오"), Elem::Number(21095.0));
    }

    #[test]
    fn parse_test() {
        let calc = KorCalc::new();

        let parsed = calc.parse("십일아무것칠");
        assert_eq!(parsed, None);

        let parsed = calc.parse("십일더하기칠");
        assert_eq!(parsed, Some(vec![Elem::Number(11.0), Elem::Op(|a, b| a + b), Elem::Number(7.0)]));

        let parsed = calc.parse("십일더하기칠빼기오");
        assert_eq!(parsed, Some(vec![
            Elem::Number(11.0), Elem::Op(|a, b| a + b), Elem::Number(7.0),
            Elem::Op(|a, b| a - b), Elem::Number(5.0)
        ]));

        let parsed = calc.parse("십일 더하기 칠 빼기 오");
        assert_eq!(parsed, Some(vec![
            Elem::Number(11.0), Elem::Op(|a, b| a + b), Elem::Number(7.0),
            Elem::Op(|a, b| a - b), Elem::Number(5.0)
        ]));

        let parsed = calc.parse("사십빼기육십더하기오십");
        assert_eq!(parsed, Some(vec![
            Elem::Number(40.0), Elem::Op(|a, b| a - b), Elem::Number(60.0),
            Elem::Op(|a, b| a + b), Elem::Number(50.0)
        ]));
    }

    #[test]
    fn evaluate_test() {
        let calc = KorCalc::new();

        if let None = calc.parse("십일 더하기 칠 빼기 아무것") {
        } else {
            assert!(false, "Parsing failed");
        }

        if let Some(parsed) = calc.parse("십일 더하기 칠 빼기 오") {
            assert_eq!(calc.evaluate(parsed), Some(13.0));
        } else {
            assert!(false, "Parsing failed");
        }

        if let Some(parsed) = calc.parse("사십빼기육십더하기오십곱하기삼나누기칠") {
            match calc.evaluate(parsed) {
                Some(result) => {
                    assert!((result - 12.85714285).abs() < 0.01);
                },
                _ => { assert!(false, "Evaluation failed"); }
            }
        } else {
            assert!(false, "Parsing failed");
        }

        if let Some(parsed) = calc.parse("오만사천칠백오십이 곱하기 삼 나누기 팔십칠 더하기 구십구 빼기 천백십") {
            match calc.evaluate(parsed) {
                Some(result) => {
                    assert!((result - 877.0).abs() < 0.01);
                },
                _ => { assert!(false, "Evaluation failed"); }
            }
        } else {
            assert!(false, "Parsing failed");
        }
    }

    #[test]
    fn compute_test() {
        let calc = KorCalc::new();
        assert!((calc.compute("오만사천칠백오십이 곱하기 삼 나누기 팔십칠 더하기 구십구 빼기 천백십") - 877.0).abs() < 0.01);
    }
}
