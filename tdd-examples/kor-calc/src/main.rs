mod lib;
use lib::KorCalc;
use std::io::{self, Read};

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    handle.read_to_string(&mut buffer)?;

    let buffer = buffer.replace("\n", "");
    let calc = KorCalc::new();
    println!("{}", calc.compute(&buffer));

    Ok(())
}
