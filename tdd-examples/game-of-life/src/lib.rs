
type Size = (usize, usize);

#[derive(Debug)]
#[derive(Clone)]
#[derive(PartialEq)]
pub enum Status { Dead, Alive }

pub struct GameOfLife {
    size: Size,
    board: Vec<Vec<Status>>,
    count: Vec<Vec<u32>>,
    marker_alive: char,
    marker_dead: char,
}

impl GameOfLife {
    pub fn new(s: Size) -> GameOfLife {
        let mut game = GameOfLife {
            size: s,
            board: vec![vec![]],
            count: vec![vec![]],
            marker_alive: '*',
            marker_dead: '-',
        };

        let (rows, cols) = game.size;
        game.board = vec![vec![Status::Dead; cols]; rows];
        game.count = vec![vec![0; cols]; rows];
        game
    }

    pub fn reset(&mut self) {
        let (rows, cols) = self.size;
        for r in 0..rows {
            for c in 0..cols {
                self.board[r][c] = Status::Dead;
            }
        }
    }

    pub fn set_marker(&mut self, status: Status, marker: char) {
        match status {
            Status::Alive => { self.marker_alive = marker; },
            Status::Dead => { self.marker_dead = marker; },
        }
    }

    pub fn print(&self) {
        let (rows, cols) = self.size;
        for r in 0..rows {
            for c in 0..cols {
                let marker = if self.board[r][c] == Status::Alive {
                    self.marker_alive
                } else {
                    self.marker_dead
                };
                print!("{}", marker);
            }
            println!("");
        }
    }

    pub fn set_status(&mut self, row: usize, col: usize, status: Status) -> bool {
        let (rows, cols) = self.size;
        if row > rows-1 || col > cols-1 {
            return false;
        }
        self.board[row][col] = status;
        true
    }

    pub fn step(&mut self) -> u64 {
        let (rows, cols) = self.size;
        let mut changes = 0;

        // count neighboring cells
        for r in 0..rows {
            for c in 0..cols {
                self.count[r][c] = 0;

                for dr in vec![-1, 0, 1] {
                    for dc in vec![-1, 0, 1] {
                        if dr == 0 && dc == 0 {
                            continue;
                        }
                        let nr = (rows as i32 + r as i32 + dr) % rows as i32;
                        let nc = (cols as i32 + c as i32 + dc) % cols as i32;
                        if self.board[nr as usize][nc as usize] == Status::Alive {
                            self.count[r][c] += 1;
                        }
                    }
                }
            }
        }

        // update board status
        for r in 0..rows {
            for c in 0..cols {
                let status_next = match self.board[r][c] {
                    Status::Alive => {
                        if self.count[r][c] < 2 || self.count[r][c] > 3 {
                            changes += 1;
                            Status::Dead
                        } else {
                            Status::Alive
                        }                        
                    },
                    Status::Dead => {
                        if self.count[r][c] == 3 {
                            changes += 1;
                            Status::Alive
                        } else {
                            Status::Dead
                        }
                    }
                };
                self.board[r][c] = status_next;
            }
        }
        changes
    }
}

#[cfg(test)]
mod tests {
    use Status;
    use GameOfLife;

    #[test]
    fn instantiation_test() {
        let game = GameOfLife::new((10, 20));

        assert_eq!(game.board.len(), 10);
        assert_eq!(game.board[0].len(), 20);
    }

    #[test]
    fn set_status_test() {
        let mut game = GameOfLife::new((10, 10));
        game.reset();

        assert_eq!(game.board[0][0], Status::Dead);
        assert_eq!(game.set_status(0, 0, Status::Alive), true);
        assert_eq!(game.board[0][0], Status::Alive);
        assert_eq!(game.set_status(0, 0, Status::Dead), true);
        assert_eq!(game.board[0][0], Status::Dead);

        assert_eq!(game.set_status(10, 10, Status::Dead), false);
        assert_eq!(game.set_status(20, 20, Status::Alive), false);
    }

    #[test]
    fn reset_test() {
        let mut game = GameOfLife::new((10, 10));

        game.set_status(0, 0, Status::Alive);
        game.set_status(1, 1, Status::Alive);
        assert_eq!(game.board[0][0], Status::Alive);
        assert_eq!(game.board[1][1], Status::Alive);

        game.reset();
        assert_eq!(game.board[0][0], Status::Dead);
        assert_eq!(game.board[1][1], Status::Dead);
    }

    #[test]
    fn count_test() {
        let mut game = GameOfLife::new((10, 10));
        game.reset();

        game.set_status(3, 3, Status::Alive);
        assert_eq!(game.count[2][2], 0);
        assert_eq!(game.count[2][3], 0);
        assert_eq!(game.count[2][4], 0);

        assert_eq!(game.count[3][2], 0);
        assert_eq!(game.count[3][3], 0);
        assert_eq!(game.count[3][4], 0);

        assert_eq!(game.count[4][2], 0);
        assert_eq!(game.count[4][3], 0);
        assert_eq!(game.count[4][4], 0);

        game.step();
        assert_eq!(game.count[2][2], 1);
        assert_eq!(game.count[2][3], 1);
        assert_eq!(game.count[2][4], 1);

        assert_eq!(game.count[3][2], 1);
        assert_eq!(game.count[3][3], 0);
        assert_eq!(game.count[3][4], 1);

        assert_eq!(game.count[4][2], 1);
        assert_eq!(game.count[4][3], 1);
        assert_eq!(game.count[4][4], 1);
    }

    #[test]
    fn rule1_test() {
        // https://en.m.wikipedia.org/wiki/Conway%27s_Game_of_Life
        // 1. Any live cell with fewer than two live neighbors dies, as if by under population.
        let mut game = GameOfLife::new((10, 10));

        game.reset();
        game.set_status(3, 3, Status::Alive);

        game.step();
        assert_eq!(game.board[3][3], Status::Dead);

        game.reset();
        game.set_status(3, 3, Status::Alive);
        game.set_status(3, 4, Status::Alive);
        game.set_status(3, 5, Status::Alive);

        game.step();
        assert_eq!(game.board[3][3], Status::Dead);
        assert_eq!(game.board[3][4], Status::Alive);
        assert_eq!(game.board[3][5], Status::Dead);
    }

    #[test]
    fn rule2_test() {
        // 2. Any live cell with two or three live neighbors lives on to the next generation.
        let mut game = GameOfLife::new((10, 10));

        // *--
        // -**
        // --*
        game.reset();
        game.set_status(2, 2, Status::Alive);
        game.set_status(3, 3, Status::Alive);
        game.set_status(3, 4, Status::Alive);
        game.set_status(4, 4, Status::Alive);

        game.step();
        assert_eq!(game.board[3][3], Status::Alive);
    }

    #[test]
    fn rule3_test() {
        // 3. Any live cell with more than three live neighbors dies, as if by overpopulation.
        let mut game = GameOfLife::new((10, 10));

        // *-*
        // -**
        // --*
        game.reset();
        game.set_status(2, 2, Status::Alive);
        game.set_status(2, 4, Status::Alive);
        game.set_status(3, 3, Status::Alive);
        game.set_status(3, 4, Status::Alive);
        game.set_status(4, 4, Status::Alive);

        game.step();
        assert_eq!(game.board[3][3], Status::Dead);
    }

    #[test]
    fn rule4_test() {
        // 4. Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
        let mut game = GameOfLife::new((10, 10));

        // *-*
        // ---
        // --*
        game.reset();
        game.set_status(2, 2, Status::Alive);
        game.set_status(2, 4, Status::Alive);
        game.set_status(4, 4, Status::Alive);

        game.step();
        assert_eq!(game.board[3][3], Status::Alive);

        game.print();
    }
}
