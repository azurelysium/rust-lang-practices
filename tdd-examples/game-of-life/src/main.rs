mod lib;
use lib::{GameOfLife, Status};

extern crate getopts;
use getopts::Options;
use std::env;

extern crate rand;
use rand::prelude::*;
use std::{thread, time};
use std::process::Command;

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help");

    opts.optopt("r", "rows", "rows of the board", "ROWS");
    opts.optopt("c", "cols", "cols of the board", "COLS");
    opts.optopt("d", "dead", "marker character of dead cells", "DEAD");
    opts.optopt("a", "alive", "marker character of alive cells", "ALIVE");
    opts.optopt("p", "prob", "probability of alive cells in the initial state", "PROB");
    opts.optopt("s", "sleep", "sleep (msecs) between game steps", "SLEEP");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m },
        Err(f) => { panic!(f.to_string()) },
    };

    // print help message
    if matches.opt_present("h") {
        let brief = format!("Usage: {} [options]", program);
        print!("{}", opts.usage(&brief));
        return;
    }

    // get parameters
    let rows = match matches.opt_str("r") {
        Some(s) => s.parse::<usize>().unwrap(),
        None => 10,
    };
    let cols = match matches.opt_str("c") {
        Some(s) => s.parse::<usize>().unwrap(),
        None => 10,
    };

    let marker_dead = match matches.opt_str("d") {
        Some(s) => s.chars().collect::<Vec<char>>()[0],
        None => '-',
    };
    let marker_alive = match matches.opt_str("a") {
        Some(s) => s.chars().collect::<Vec<char>>()[0],
        None => '*',
    };

    let prob_alive = match matches.opt_str("p") {
        Some(s) => s.parse::<f64>().unwrap(),
        None => 0.1,
    };
    let duration = time::Duration::from_millis(match matches.opt_str("s") {
        Some(s) => s.parse::<u64>().unwrap(),
        None => 1000,
    });

    // initiate the game
    let mut game = GameOfLife::new((rows, cols));
    game.set_marker(Status::Alive, marker_alive);
    game.set_marker(Status::Dead, marker_dead);

    // set initial state
    let mut rng = thread_rng();
    for r in 0..rows {
        for c in 0..cols {
            if rng.gen::<f64>() < prob_alive {
                game.set_status(r, c, Status::Alive);
            } else {
                game.set_status(r, c, Status::Dead);
            }
        }
    }

    loop {
        game.print();
        if game.step() == 0 {
            return;
        }

        // clear screen and sleep
        print!("{}[2J", 27 as char);
        thread::sleep(duration);
    }
}
