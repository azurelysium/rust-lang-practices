fn encode_spaces(s:&mut String) {
    let len = s.trim_right().len();
    let mut bytes = s.clone().into_bytes();

    let mut spaces = 0;
    for i in 0..len {
        if bytes[i] == b' ' {
            spaces += 1;
        }
    }

    let mut offset = len - 1 + (spaces * 2);

    for i in (0..len).rev() {
        if bytes[i] == b' ' {
            bytes[offset-0] = b'0';
            bytes[offset-1] = b'2';
            bytes[offset-2] = b'%';
            offset -= 3;
        } else {
            bytes[offset] = bytes[i];
            if i > 0 {
                offset -= 1;
            }
        }
    }

    *s = String::from_utf8(bytes).unwrap();
}

fn main() {
    let mut s = String::from("I am your father.                    ");
    encode_spaces(&mut s);
    println!("{}", s);
}
