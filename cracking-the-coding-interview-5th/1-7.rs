use std::collections::HashSet;

struct Matrix {
    n: usize,
    mat: Vec<Vec<u32>>,
}

impl Matrix {
    fn new(n: usize) -> Matrix {
        let mut mat = vec![vec![0; n]; n];

        let mut cnt = 1;
        for i in 0..n {
            for j in 0..n {
                mat[i][j] = cnt;
                cnt += 1;
            }
        }

        Matrix {n, mat}
    }

    fn print(&self) {
        for i in 0..self.n {
            for j in 0..self.n {
                print!("{:4} ", self.mat[i][j]);
            }
            println!("");
        }
    }

    fn expand_zero_to_rowcol(&mut self) {
        let mut rows = HashSet::new();
        let mut cols = HashSet::new();

        for i in 0..self.n {
            for j in 0..self.n {
                if self.mat[i][j] == 0 {
                    rows.insert(i);
                    cols.insert(j);
                }
            }
        }

        for row in rows.iter() {
            for j in 0..self.n {
                self.mat[*row as usize][j] = 0;
            }
        }

        for col in cols.iter() {
            for i in 0..self.n {
                self.mat[i][*col as usize] = 0;
            }
        }
    }
}

fn main() {
    let mut mat = Matrix::new(5);
    mat.mat[1][1] = 0;
    mat.mat[1][3] = 0;

    mat.print();
    println!("");
    
    mat.expand_zero_to_rowcol();

    mat.print();
    println!("");
}
