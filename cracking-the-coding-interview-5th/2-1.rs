use std::mem;

struct Node {
    value: i32,
    prev: Option<Box<Node>>,
    next: Option<Box<Node>>,
}

impl Node {
    fn new(value: i32) -> Node {
        Node { value, prev: None, next: None }
    }
}

struct LinkedList {
    head: Option<Box<Node>>,
}

impl LinkedList {
    fn new() -> LinkedList {
        LinkedList { head: None }
    }

    fn push_back(&mut self, value: i32) {
        let node = Node::new(value);

        match self.head {
            None => { self.head = Some(Box::new(node)); },
            Some(ref mut head_boxed) => {
                let mut curr: &mut Box<Node> = head_boxed;
                loop {
                    if let Some(ref mut next_boxed) = curr.next {
                        mem::replace(curr, next_boxed);
                    } else {
                        curr.next = Some(Box::new(node));
                        break;
                    }
                }
            },
        }
    }

    fn print_all(&self) {
        if let Some(ref head_boxed) = self.head {
            let mut curr = head_boxed;
            loop {
                println!("{}", curr.value);
                if let Some(ref next_boxed) = curr.next {
                    curr = next_boxed;
                } else {
                    break;
                }
            }
        }
    }
}

fn main() {
    let mut ll = LinkedList::new();
    ll.push_back(123);
    ll.push_back(456);
    ll.print_all();
}
