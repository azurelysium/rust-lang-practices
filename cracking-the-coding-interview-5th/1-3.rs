fn char_count_vec(s: String) -> String {
    let mut arr: [i32; 256] = [0; 256];
    for c in s.into_bytes() {
        arr[c as usize] += 1;
    }
    arr.iter().map(|c| c.to_string()).collect::<Vec<String>>().join(",")
}

fn is_anagram(s1: &str, s2: &str) -> bool {
    let vec1 = char_count_vec(s1.to_lowercase());
    let vec2 = char_count_vec(s2.to_lowercase());
    vec1 == vec2
}

fn main() {
    let s1 = "forty five";
    let s2 = "over fifty";
    println!("{}, {} => {}", s1, s2, is_anagram(&s1, &s2));

    let s1 = "Madam Curie";
    let s2 = "Radium came";
    println!("{}, {} => {}", s1, s2, is_anagram(&s1, &s2));
}
