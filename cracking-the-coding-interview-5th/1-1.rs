use std::collections::HashMap;

fn is_all_unique_chars(s: &str) -> bool {
    let mut charmap = HashMap::new();
    
    for c in s.chars() {
        if charmap.contains_key(&c) {
            return false;
        }
        charmap.insert(c, true);
    }
    true
}

fn is_all_unique_chars_2(s: &str) -> bool {
    let mut charmap: [bool; 256] = [false; 256];
    for c in s.chars() {
        let idx = c as usize;
        if charmap[idx] == true{
            return false;
        }
        charmap[idx] = true;
    }
    true
}

fn main() {
    let s = "hello";
    println!("{} => {}", s, is_all_unique_chars(&s));

    let s = "world";
    println!("{} => {}", s, is_all_unique_chars_2(&s));
}
