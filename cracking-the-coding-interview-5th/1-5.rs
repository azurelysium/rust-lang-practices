fn compress(s: String) -> String {
    if s.len() == 0 {
        return s;
    }

    let bytes = s.clone().into_bytes();
    let mut compressed = String::from("");

    let mut start = 0;
    let mut ch = bytes[0];

    for i in 0..bytes.len() {
        if bytes[i] == ch {
            continue;
        }
        compressed.push_str(&format!("{}{}", String::from_utf8(vec!(ch)).unwrap(), i-start));
        start = i;
        ch = bytes[i];
    }
    compressed.push_str(&format!("{}{}", String::from_utf8(vec!(ch)).unwrap(), bytes.len()-start));

    if compressed.len() < s.len() {
        compressed
    } else {
        s
    }
}

fn main() {
    let s = String::from("abbcccddddeeeeeffffff");
    println!("{}", compress(s));

    let s = String::from("abcdefg");
    println!("{}", compress(s));
}
