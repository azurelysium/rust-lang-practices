fn is_rotated(s1: &str, s2: &str) -> bool {
    let doubled = format!("{}{}", s1, s1);
    if let Some(_) = doubled.find(s2) {
        true
    } else {
        false
    }
}

fn main() {
    let s1 = "waterbottLe";
    let s2 = "erbottLewat";
    println!("Can '{}' be made by rotating '{}'? => {} ", s2, s1, is_rotated(s1, s2));

    let s1 = "waterbottle";
    let s2 = "erbottLewat";
    println!("Can '{}' be made by rotating '{}'? => {} ", s2, s1, is_rotated(s1, s2));
}
