fn reverse(s: &mut String) {
    let mut chars = s.clone().into_bytes();
    let len = chars.len();
    
    for i in 0..len/2 {
        let c = chars[len-1-i];
        chars[len-1-i] = chars[i];
        chars[i] = c;
    }

    *s = String::from_utf8(chars).unwrap();
}

fn main() {
    let mut s = String::from("Hello, world");
    reverse(&mut s);
    println!("{:?}", s);
}
