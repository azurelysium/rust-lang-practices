use std::collections::HashMap;

struct Square {
    n: usize,
    mat: Vec<Vec<u32>>,
}

impl Square {
    fn new(n: usize) -> Square {
        let mut mat = vec![vec![0; n]; n];

        let mut cnt = 1;
        for i in 0..n {
            for j in 0..n {
                mat[i][j] = cnt;
                cnt += 1;
            }
        }

        Square {n, mat}
    }

    fn print(&self) {
        for i in 0..self.n {
            for j in 0..self.n {
                print!("{} ", self.mat[i][j]);
            }
            println!("");
        }
    }

    fn rotate(&mut self) {
        let mut mapping: HashMap<(usize, usize), (usize, usize)> = HashMap::new();

        for i in 0..self.n {
            for j in 0..self.n {

                let from = (i, j);
                let to = Square::_destination(from, self.n);

                let from_at = match mapping.get(&from) {
                    Some(value) => *value,
                    None => from,
                };

                let to_at = match mapping.get(&to) {
                    Some(value) => *value,
                    None => to,
                };

                Square::_swap(&mut self.mat, from_at, to);

                mapping.insert(from_at, to);
                mapping.insert(to_at, from_at);
            }
        }
    }

    fn _destination(from: (usize, usize), len: usize) -> (usize, usize) {
        let (i, j) = from;
        (len-1-j, i)
    }

    fn _swap(mat: &mut Vec<Vec<u32>>, from: (usize, usize), to: (usize, usize)) {
        let temp = mat[to.0][to.1];
        mat[to.0][to.1] = mat[from.0][from.1];
        mat[from.0][from.1] = temp;
    }
}

fn main() {
    let mut image = Square::new(3);

    image.print();
    println!("");

    image.rotate();
    image.print();
    println!("");

    image.rotate();
    image.print();
    println!("");

    image.rotate();
    image.print();
    println!("");

    image.rotate();
    image.print();
    println!("");
}
