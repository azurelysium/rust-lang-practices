#![windows_subsystem = "windows"]

extern crate gtk;
extern crate lopdf;

use std::fs;

use lopdf::Document;

use gtk::prelude::*;
use gtk::{Button, Box, Image, Window, WindowType, MessageDialog};
use gtk::{FileChooserDialog, FileFilter, FileChooserAction, ResponseType};
use gtk::{DialogFlags, MessageType, ButtonsType};

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    match fs::create_dir("pages") {
        _ => ()
    }

    let window = Window::new(WindowType::Toplevel);
    window.set_title("Hyegu PDF");
    window.set_default_size(300, 100);

    let container = Box::new(gtk::Orientation::Horizontal, 10);

    let image = Image::new_from_file("hyegu.png");
    container.pack_start(&image, false, false, 0);

    let button = Button::new_with_label("PDF 문서 선택하기");
    container.pack_start(&button, true, true, 0);

    window.add(&container);
    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    button.connect_clicked(move |_| {
        let dialog = FileChooserDialog::with_buttons::<Window>(
            Some("Open File"),
            Some(&window),
            FileChooserAction::Open,
            &[("_Cancel", ResponseType::Cancel), ("_Open", ResponseType::Accept)]
        );

        let filter = FileFilter::new();
        filter.add_pattern("*.pdf");
        gtk::FileFilterExt::set_name(&filter, "PDF 문서");
        dialog.add_filter(&filter);

        let mut n_pages = 0;
        if dialog.run() == ResponseType::Accept.into() {
            let filename = dialog.get_filename().expect("Couldn't get filename");
            println!("Filename: {:?}", filename);

            let doc = Document::load(&filename).unwrap();
            n_pages = *doc.get_pages().keys().max().unwrap_or(&0);
            println!("Total pages: {:?}", n_pages);

            for page_no in 1..n_pages+1 {
                let mut doc_new = Document::load(&filename).unwrap();
                let mut pages_to_delete = (1..n_pages+1).filter(|&i| i != page_no).collect::<Vec<_>>();
                doc_new.delete_pages(&pages_to_delete);
                doc_new.save(format!("pages/{}.pdf", page_no)).unwrap();
            }
        }
        dialog.destroy();

        let msg_diag = MessageDialog::new(
            Some(&window),
            DialogFlags::MODAL,
            MessageType::Info,
            ButtonsType::Ok,
            &format!("{}쪽 저장됨", n_pages));

        msg_diag.run();
        msg_diag.destroy();
    });

    gtk::main();
}
