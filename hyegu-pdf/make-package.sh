#!/bin/bash
GTK_INSTALL_PATH=/opt/gtkwin
DESTINATION=/home/azurelysium/Works/hyegu-pdf/packaged

rm -rf $DESTINATION
mkdir $DESTINATION

cp target/x86_64-pc-windows-gnu/release/*.exe $DESTINATION
cp $GTK_INSTALL_PATH/bin/*.dll $DESTINATION
mkdir -p $DESTINATION/share/glib-2.0/schemas
mkdir $DESTINATION/share/icons
cp $GTK_INSTALL_PATH/share/glib-2.0/schemas/* $DESTINATION/share/glib-2.0/schemas
cp -r $GTK_INSTALL_PATH/share/icons/* $DESTINATION/share/icons
