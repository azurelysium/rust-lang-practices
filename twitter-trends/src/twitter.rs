extern crate rand;
extern crate url;
extern crate base64;
extern crate crypto;
extern crate reqwest;
extern crate serde_json;
extern crate percent_encoding;

use std::fmt;
use std::vec::Vec;
use std::time::{SystemTime, UNIX_EPOCH};
use std::collections::HashMap;
use std::io::Read;
use self::rand::{thread_rng, Rng, ThreadRng};
use self::url::Url;
use self::serde_json::Value;
use self::crypto::hmac::Hmac;
use self::crypto::mac::Mac;
use self::crypto::digest::Digest;
use self::crypto::sha1::Sha1;
use self::url::percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};

pub struct TwitterApi<'a> {
    verbose: bool,
    consumer_key: &'a str,
    consumer_secret: &'a str,
    token_key: &'a str,
    token_secret: &'a str,
    client: reqwest::Client,
    rng: ThreadRng,
}

impl<'a> TwitterApi<'a> {
    pub fn new(consumer_key: &'a str, consumer_secret: &'a str,
               token_key: &'a str, token_secret: &'a str) -> TwitterApi<'a> {

        let mut rng = thread_rng();
        let mut client = reqwest::Client::new();
        TwitterApi { consumer_key, consumer_secret, token_key, token_secret,
                     client, rng, verbose: false }
    }

    pub fn set_verbose(&mut self, verbose: bool) {
        self.verbose = verbose;
    }

    fn generate_nonce(&mut self) -> String {
        let choices = String::from("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        let choices_arr = choices.as_bytes();

        let mut nonce = Vec::new();
        for _ in 0..32 {
            nonce.push(self.rng.choose(choices_arr).unwrap().to_owned());
        }
        String::from_utf8(nonce).unwrap()
    }

    fn url_encode(input: &str) -> String {
        let encoded = utf8_percent_encode(input, DEFAULT_ENCODE_SET).to_string();
        let encoded = encoded
            .replace(":", "%3A")
            .replace("/", "%2F")
            .replace("=", "%3D")
            //.replace(",", "%2C")
            .replace("+", "%2B")
            .replace("&", "%26");
            //.replace("%", "%25");
        encoded
    }

    fn make_encoded_param_string(pairs: &Vec<(&String, &Value)>) -> String {
        let encoded_pairs: Vec<String> = pairs.into_iter().map(
            |(k, v)| format!("{}={}",
                             utf8_percent_encode(k, DEFAULT_ENCODE_SET),
                             utf8_percent_encode(&v.to_string().replace("\"", ""), DEFAULT_ENCODE_SET))).collect();
        let encoded = encoded_pairs.join("&");
        String::from(encoded)
    }

    fn get_sorted_pairs<'b>(values: &[&'b Value]) -> Vec<(&'b String, &'b Value)> {
        let mut pairs = Vec::new();
        for val in values {
            for (k, v) in val.as_object().unwrap().iter() {
                pairs.push((k, v));
            }
        }
        pairs.sort_by_key(|k| k.0);
        pairs
    }

    fn get_auth_headers(&mut self, method: &str, endpoint: &str,
                        body: &Value, params: &Value) -> HashMap<String, String> {
        let mut authorization_params = json!({
            "oauth_consumer_key": self.consumer_key,
            "oauth_nonce": self.generate_nonce(),
            "oauth_signature_method": "HMAC-SHA1",
            "oauth_timestamp": SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs().to_string(),
            "oauth_token": self.token_key,
            "oauth_version": "1.0",
        });

        let signature: String;
        {
            let pairs = TwitterApi::get_sorted_pairs(&[&authorization_params, body, params]);
            let encoded = TwitterApi::make_encoded_param_string(&pairs);
            println!("encoded: {:?}\n", encoded);

            // Construct a string to be signed
            let signature_base_string = format!("{}&{}&{}", method,
                                                TwitterApi::url_encode(endpoint),
                                                TwitterApi::url_encode(&encoded));

            println!("signature_base_string: {:?}\n", signature_base_string);

            let signing_key = format!("{}&{}", self.consumer_secret, self.token_secret);

            println!("signing_key: {:?}\n", signing_key);
            
            let mut hmac = Hmac::new(Sha1::new(), signing_key.as_bytes());
            hmac.input(signature_base_string.as_bytes());
            signature = TwitterApi::url_encode(&base64::encode(&hmac.result().code()));
        }

        authorization_params["oauth_signature"] = json!(signature);
        let pairs = TwitterApi::get_sorted_pairs(&[&authorization_params]);

        let encoded_pairs: Vec<String> = pairs.into_iter().map(
            |(k, v)| format!("{}={}", k, &v.to_string())).collect();
        let encoded = encoded_pairs.join(", ");

        let mut headers = HashMap::new();
        headers.insert(String::from("Authorization"), format!("OAuth {}", encoded));
        headers.insert(String::from("User-Agent"), String::from("OAuth gem v0.4.4"));
        headers.insert(String::from("Content-Type"), String::from("application/x-www-form-urlencoded"));

        headers
    }

    pub fn search_tweets(&mut self, query: &str, count: i32) -> Value {
        let url = "https://api.twitter.com/1.1/search/tweets.json";
        let body = json!({});
        let params = json!({
            "q": query,
            "lang": "ko",
            "result_type": "recent",
            "count": count,
        });

        let auth_headers = self.get_auth_headers("GET", url, &body, &params);
        let mut headers = reqwest::header::Headers::new();
        for (k, v) in auth_headers.iter() {
            headers.set_raw(k.to_string(), v.to_string());
        }

        let pairs = TwitterApi::get_sorted_pairs(&[&params]);
        let encoded = TwitterApi::make_encoded_param_string(&pairs);

        if self.verbose {
            println!("");
            println!("Url: {}", url);
            println!("Headers: {:?}", &headers);
            println!("Params: {}", &encoded);
            println!("");
        }

        let url_with_params = format!("{}?{}", url, encoded);
        let mut req = self.client.request(reqwest::Method::Get, &url_with_params);
        let mut req = req.headers(headers);

        let mut resp = req.send().unwrap();
        let resp_status = resp.status();
        let resp_text = resp.text().unwrap();

        if self.verbose {
            println!("");
            println!("Resp: ({:?}) {:?}", resp_status, resp_text);
            println!("");
        }

        assert!(resp_status.is_success());
        serde_json::from_str(&resp_text).unwrap()
    }
}
