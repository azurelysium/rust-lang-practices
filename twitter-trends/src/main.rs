extern crate twitter_stream;
extern crate cursive;

#[macro_use]
extern crate serde_json;

use std::{thread, time};
use std::collections::VecDeque;
use std::sync::{Mutex, Arc};

use self::serde_json::Value;

use cursive::Cursive;
use cursive::traits::*;
use cursive::views::{BoxView, ListView, DummyView, LinearLayout, SelectView, TextView, TextContent};

use twitter_stream::{Token, TwitterStreamBuilder};
use twitter_stream::rt::{self, Future, Stream};

mod twitter;
use twitter::TwitterApi;

const WORDS_MAX: usize = 500;

fn main() {

    let mut words: VecDeque<String> = VecDeque::new();
    let arc_words = Arc::new(Mutex::new(words));

    let consumer_key = "g61G7H5PLc9P3PDIWNY2IGKFF";
    let consumer_secret = "gm4U7ZlECt1MkGFhbDfMsta8d8j08rKwAD41jXroBhXFsiJU7f";
    let token_key = "1011886245444587520-fuLBOLVYg8WGOdgO8nhUD0T1avver0";
    let token_secret = "oqNYFyOHROBqCQsSs58nwjV2VgnkcKyi8XeLX1ks3yBCk";

    // API to search tweets
    let mut api = TwitterApi::new(consumer_key, consumer_secret, token_key, token_secret);
    api.set_verbose(true);

    /*
    let query = "bitcoin";
    let resp = api.search_tweets(query, 10);
    let statuses = &resp["statuses"];
    for status in statuses.as_array().unwrap() {
        println!("{}", status["text"]);
    }
     */

    // Construct user interface
    let mut app = Cursive::default();
    app.add_global_callback('q', |s| s.quit());

    let mut content_tweet = TextContent::new("Recent Tweet");

    app.add_layer(LinearLayout::vertical()
                  .child(BoxView::with_full_screen(TextView::new_with_content(content_tweet.clone())
                                                   .scrollable(true).with_id("tweet")))
                  .child(LinearLayout::horizontal()
                         .child(BoxView::with_full_screen(ListView::new().with_id("keywords")))
                         .child(BoxView::with_full_screen(TextView::new("Search Result")
                                                         .with_id("search_result"))))
    );

    {
        let arc_words = Arc::clone(&arc_words);
        app.add_global_callback('c', move |s| {
            let mut words = arc_words.lock().unwrap();
            //println!("#words: {}", words.len());
        });
    }

    // Get real-time tweet stream
    {
        let arc_words = Arc::clone(&arc_words);

        thread::spawn(move || {
            let three_secs = time::Duration::from_secs(3);
            thread::sleep(three_secs);

            let token = Token::new(consumer_key, consumer_secret, token_key, token_secret);
            let future = TwitterStreamBuilder::sample(&token)
                .listen()
                .flatten_stream()
                .for_each(move |json| {
                    let mut words = arc_words.lock().unwrap();

                    let obj: Value = serde_json::from_str(&json).unwrap();
                    if obj["lang"] == "ko" {
                        let text = obj["text"].as_str().unwrap();
                        //println!("{}", text);

                        content_tweet.set_content(text);

                        let iter = text.split_whitespace();
                        let tokens: Vec<&str> = iter.collect();
                        for tok in tokens {
                            words.push_back(String::from(tok));
                        }
                        while words.len() > WORDS_MAX {
                            words.pop_front();
                        }
                    }
                    Ok(())
                })
                .map_err(|e| println!("error: {}", e));
            rt::run(future);
        });
    }

    // run UI loop
    app.run();
}
